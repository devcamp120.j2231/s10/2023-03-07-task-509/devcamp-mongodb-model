//Import thư viện express
const express = require("express");

// Khởi tạo ứng dụng express
const app = express();

// Khai báo cổng chạy project
const port = 8000;

//Import thư viện mongoose
const mongoose = require("mongoose");

// Import router
const courseRouter = require("./app/routes/course.router");
const reviewRouter = require("./app/routes/review.router");

// Khai báo middleware đọc được request body json
app.use(express.json());

// Khai báo middleware in ra console thời gian hiện tại
app.use((req, res, next) => {
    console.log("Time: ", new Date());
    // Gọi hàm next để sang bước kế tiếp
    next();
})

// Khai báo middleware in ra console request method
app.use((req, res, next) => {
    console.log("Request method: ", req.method);
    next();
})

// Kết nối với mongodb
mongoose.connect("mongodb://127.0.0.1:27017/CourseDatabase", (err) => {
    if(err) throw err;

    console.log("Connect MongoDB Successfully!")
})

// Khai báo API router /
app.get("/", (req, res) => {
    res.status(200).json({
        message: "Devcamp Middleware"
    })
})

// Khai báo api cho router /courses
app.use("/api", courseRouter);
// Khai báo api cho router /reviews
app.use("/api", reviewRouter);

// Chạy project lắng nghe cổng port
app.listen(port, () => {
    console.log("App listening on port: ", port);
})

