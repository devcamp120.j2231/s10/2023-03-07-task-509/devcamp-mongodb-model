// Import thư viện express
const express = require("express");

const router = express.Router();

// Import course middleware sang
const courseMiddleware = require("../middlewares/course.middleware");

// Import course controller sang
const courseController = require("../controllers/course.controller");

// router.use(courseMiddleware.courseRouterMiddleware);

// Get all
router.get("/courses",
    courseMiddleware.courseGetAllMiddleware, 
    courseController.getAllCourse);

// Create
router.post("/courses", 
    courseMiddleware.courseCreateMiddleware, 
    courseController.createCourse);

// Get detail
router.get("/courses/:courseid", 
    courseMiddleware.courseGetDetailMiddleware, 
    courseController.getCourseById);

// Update
router.put("/courses/:courseid", 
    courseMiddleware.courseUpdateMiddleware, 
    courseController.updateCourseById);

// Delete
router.delete("/courses/:courseid", 
    courseMiddleware.courseDeleteMiddleware, 
    courseController.deleteCourseById);

// Export router thành 1 module
module.exports = router;