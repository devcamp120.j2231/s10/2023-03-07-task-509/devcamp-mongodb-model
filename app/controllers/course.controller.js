const mongoose = require("mongoose");
const courseModel = require("../models/course.model");

const createCourse = (req, res) => {
    // B1: Thu thập dữ liệu từ request
    const { reqTitle, reqDescription, reqStudent } = req.body;
    // console.log(body); { reqTitle: 'R31', reqDescription: 'ReactJs - NodeJS', reqStudent: 20 }

    // B2: Validate dữ liệu
    if(!reqTitle) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Title required!"
        })
    }

    if( !( Number.isInteger(reqStudent) && reqStudent >= 0 ) ) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Number student is not valid"
        })
    }

    // B3: Gọi model thao tác với CSDL
    var newCourse = {
        title: reqTitle,
        description:reqDescription,
        noStudent: reqStudent
    }

    courseModel.create(newCourse, (err, data) => {
        if(err) {
            return res.status(500).json({
                status: "Interal server error",
                err: err.message
            })
        }

        return res.status(201).json({
            status: "Create successfully",
            data: data
        })
    });
}

const getAllCourse = (req, res) => {
    // B1: Thu thập dữ liệu từ request
    // B2: Validate dữ liệu
    // B3: Gọi model thao tác với CSDL
    courseModel.find().exec((err, data) => {
        if(err) {
            return res.status(500).json({
                status: "Interal server error",
                err: err.message
            })
        }

        return res.status(200).json({
            status: "Get all courses successfully",
            data: data
        })
    });
}

const getCourseById = (req, res) => {
    // B1: Thu thập dữ liệu từ request
    const courseid = req.params.courseid;

    // B2: Validate dữ liệu
    if( !mongoose.Types.ObjectId.isValid(courseid) ) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Course Id is not valid"
        })
    }

    // B3: Gọi model thao tác với CSDL
    courseModel.findById(courseid).exec((err, data) => {
        if(err) {
            return res.status(500).json({
                status: "Interal server error",
                err: err.message
            })
        }

        if(data) {
            return res.status(200).json({
                status: "Get detail successfully",
                data: data
            })
        } else {
            return res.status(404).json({
                status: "Not Found"
            })
        }

       
    })
}

const updateCourseById = (req, res) => {
    // B1: Thu thập dữ liệu từ request
    const courseid = req.params.courseid;
    const { reqTitle, reqDescription, reqStudent } = req.body;

    // B2: Validate dữ liệu
    if( !mongoose.Types.ObjectId.isValid(courseid) ) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Course Id is not valid"
        })
    }

    // Khi update không truyền đầy đủ các trường thì vẫn hợp lệ
    // Khi không truyền trường nào thì trường đó sẽ có giá trị là undefined
    if( reqTitle !== undefined && reqTitle === "" ) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Title is not valid"
        })
    }

    if( reqStudent !== undefined && !( Number.isInteger(reqStudent) && reqStudent >= 0 ) ) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Number student is not valid"
        })
    }

    // B3: Gọi model thao tác với CSDL
    var courseUpdate = {
        title: reqTitle,
        description: reqDescription,
        noStudent: reqStudent
    }

    courseModel.findByIdAndUpdate(courseid, courseUpdate, (err, data) => {
        if(err) {
            return res.status(500).json({
                status: "Interal server error",
                err: err.message
            })
        }

        if(data) {
            return res.status(200).json({
                status: "Update successfully",
                data: data
            })
        } else {
            return res.status(404).json({
                status: "Not Found"
            })
        }
    })
}

const deleteCourseById = (req, res) => {
    // B1: Thu thập dữ liệu từ request
    const courseid = req.params.courseid;

    // B2: Validate dữ liệu
    if( !mongoose.Types.ObjectId.isValid(courseid) ) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Course Id is not valid"
        })
    }

    // B3: Gọi model thao tác với CSDL
    courseModel.findByIdAndDelete(courseid, (err, data) => {
        if(err) {
            return res.status(500).json({
                status: "Interal server error",
                err: err.message
            })
        }

        if(data) {
            return res.status(204).json()
        } else {
            return res.status(404).json({
                status: "Not Found"
            })
        }   
    });
}

module.exports = {
    createCourse,
    getAllCourse,
    getCourseById,
    updateCourseById,
    deleteCourseById
}