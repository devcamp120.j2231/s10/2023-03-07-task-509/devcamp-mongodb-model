//Import thư viện mongoose
const mongoose = require("mongoose");

// Khai báo class Schema
const Schema = mongoose.Schema;

// Khởi tạo course Schema
const courseSchema = new Schema({
    title: {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String,
        required: false
    },
    noStudent: {
        type: Number,
        defaule: 0
    },
    reviews: [{
        type: mongoose.Types.ObjectId, // ObjectId là kiểu dữ liệu của trường _id
        ref: "Review"
    }] 
}, {
    timestamps: true
});

// Biên dịch Course model từ course Schema
module.exports = mongoose.model("Course", courseSchema);